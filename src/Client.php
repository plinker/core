<?php
namespace plinker\Core;

use Requests;

class Client
{
    /**
     * @param string $url
     * @param string $component
     * @param string $publicKey
     * @param string $privateKey
     * @config array $config
     */
    public function __construct(
        $url,
        $component,
        $publicKey = '',
        $privateKey = '',
        $config = array()
    ) {
        $this->endpoint = $url;
        $this->component = $component;
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
        $this->config = $config;
    }

    /**
     * Helper which changes the server component on the fly without changing
     * the connection
     *
     * @param string $component - component class namespace
     * @param array  $config    - component array
     */
    public function useComponent($component = '', $config = array())
    {
        $this->component = $component;
        $this->config = $config;

        return new $this(
            $this->endpoint,
            $this->component,
            $this->publicKey,
            $this->privateKey,
            $this->config
        );
    }

    /**
     * Magic caller
     *
     * @param string $action
     * @param array  $params
     */
    public function __call($action, $params)
    {
        if (!is_scalar($action)) {
            throw new Exception('Method name has no scalar value');
        }

        if (!is_array($params)) {
            throw new Exception('Params must be given as array');
        }

        $params = array_values($params);

        $signer = new Signer($this->publicKey, $this->privateKey);

        $encoded = $signer->encode(array(
            'time' => microtime(true),
            'self' => $this->endpoint,
            'component' => $this->component,
            'config' => $this->config,
            'action' => $action,
            'params' => $params
        ));

        $response = Requests::post(
            $this->endpoint,
            array(),
            $encoded,
            array(
            	'timeout' => (!empty($this->config['timeout']) ? (int) $this->config['timeout'] : 10),
            )
        );

        return $response->body;
    }

}

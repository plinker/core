<?php
namespace plinker\Core;

class Server
{
    /**
     * @param string $post
     * @param string $publicKey
     * @param string $privateKey
     * @param array  $config
     */
    public function __construct(
        $post,
        $publicKey = '',
        $privateKey = '',
        $config = array()
    ) {
        $this->post = $post;
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
        $this->config = $config;
    }

    /**
     *
     */
    public function execute()
    {
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json; charset=utf8');

        $signer = new Signer($this->publicKey, $this->privateKey);

        $data = $signer->decode(
            $this->post
        );

        if (!is_array($data)) {
            return json_encode($data, JSON_PRETTY_PRINT);
        }

        if (!isset($data['params'])) {
            $data['params'] = array();
        }

        if (!empty($data['config'])) {
            $this->config = $data['config'];
        }

        if (empty($data['component'])) {
            return json_encode('component class cannot be empty', JSON_PRETTY_PRINT);
        }

        if (empty($data['action'])) {
            return json_encode('action cannot be empty', JSON_PRETTY_PRINT);
        }

        $class = '\\plinker\\'.$data['component'];

        if (class_exists($class)) {
            $componentClass = new $class($this->config+$data+$this->post);

            if (method_exists($componentClass, $data['action'])) {
                $return = call_user_func(
                    array(
                        $componentClass,
                        $data['action']
                    ),
                    $data['params']
                );
            } else {
                $return = 'action not implemented';
            }
        } else {
            $return = 'not implemented';
        }
        return json_encode($return, JSON_PRETTY_PRINT);
    }

}

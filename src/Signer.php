<?php
namespace plinker\Core;

use plinker\Base91\Base91;

class Signer {

    /**
     * Construct
     *
     * @param string $publicKey
     * @param string $privateKey
     */
    public function __construct($publicKey = null, $privateKey = null)
    {
        $this->publicKey  = $publicKey;
        $this->privateKey = $privateKey.(date('z') + 1);
    }

    /**
     * Payload encode
     * Encodes and signs the payload packet
     *
     * @param array $signer
     * @return array
     */
    public function encode($signer = array())
    {
        $data = json_encode($signer);

        $signer = array(
            'data'         => str_rot13(Base91::encode($data)),
            'public_key'   => $this->publicKey,
            'request_time' => time()
        );

        $signer['token'] = hash_hmac(
            'sha256',
            $signer['data'],
            $this->privateKey
        );

        return $signer;
    }

    /**
     * Payload decode
     * Validates and decodes payload packet
     *
     * @param array $signer
     * @return object
     */
    public function decode($signer = array())
    {
        // packet validation
        if (!$this->validatePacket($signer)) {
            return $this->packet_state;
        }

        $signer['data'] = Base91::decode(str_rot13($signer['data']));

        return json_decode($signer['data'], true);
    }

    /**
     * Validate payload packet
     *
     * @param array $signer
     * @return bool
     */
    public function validatePacket($signer = array())
    {
        $this->packet_state = 'valid';

        if (empty($signer['public_key'])) {
            $this->packet_state = 'missing public key';
            return false;
        }

        if (empty($signer['token'])) {
            $this->packet_state = 'missing token key';
            return false;
        }

        if (empty($signer['data'])) {
            $this->packet_state = 'empty data';
            return false;
        }

        if ($signer['public_key'] !== $this->publicKey) {
            $this->packet_state = 'unauthorised public key';
            return false;
        }

        // validate packet signature
        if (hash_hmac(
            'sha256',
            $signer['data'],
            $this->privateKey
        ) == $signer['token']) {
            return true;
        } else {
            $this->packet_state = 'unauthorised';
            return false;
        }
    }
}

